﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_3.Model
{
    internal class Product
    {
        public int Id { get; set; } 
        public string Name { get; set; }    
        public int Price { get; set; }
        public string Category { get; set; }

        public Product(int id,string name,int price,string category)
        {
            Id = id;
            Name = name;
            Price = price;
            Category = category;
        }


        public override string ToString()
        {
            return $"ID :{Id}  Name ::{Name} Price::{Price} Category ::{Category}";
        }
    }
}
